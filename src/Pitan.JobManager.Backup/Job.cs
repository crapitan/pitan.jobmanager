﻿using System;
using System.Collections.Generic;
using Pitan.JobManager.Requirements;
using Pitan.JobManager.Device;
using System.Linq;
using Newtonsoft.Json;

namespace Pitan.JobManager
{
    public abstract class Job
    {
        private JobParameter jobParameter;

        private long persistentId;
        private int runIteration;
        
        public Job(JobParameter jobParameter, IDeviceSpecific device)
        {
            this.jobParameter = jobParameter;
            this.Device = device;
        }

        #region Proerpertis

        [JsonIgnore]
        public EncryptionKeys EncryptionKeys { get; set; }

        public IList<IRequirement> Requirements

        {
            get
            {
                return this.jobParameter?.Requrements;
            }
        }

        public string GroupId
        {
            get
            {
                return this.jobParameter?.GroupId;
            }
        }
        
        public bool IsPersistan
        {
            get
            {
                return this.jobParameter.IsPersistant;
            }
        }

        public int RetryCount
        {
            get
            {
                return this.jobParameter?.RetryCount != null ? this.jobParameter.RetryCount : 0;
            }
        }

        public bool WakeLock
        {
            get
            {
                return this.jobParameter.WakeLock;
            }
        }

        public bool IsRequrementsMet

        {
            get
            {
                return this.Requirements.Any(r => r.IsPresent() == false);
            }
        }

        public long PersistentId { get; set; }

        public int RunIteration { get; set; }

        public int WakeLockTimeOut { get; set; }

        public IDeviceSpecific Device { get; set; }
        

        #endregion

        public void Wakeup()
        {
            this.Device?.WakeUp();
        }
        
        public void Sleep()
        {
            this.Device?.GoToSleep();
        }
        
        public abstract void Run();
        public abstract void Added();
        public abstract bool ShouldRetry(Exception exp);
        public abstract void Canceled();

    }

}
