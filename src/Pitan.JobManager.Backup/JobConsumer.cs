﻿using Android.Util;
using Java.Lang;
using Pitan.JobManager.Persistance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Pitan.JobManager
{
    public class JobConsumer
    {

        private static string TAG = typeof(JobConsumer).ToString();
        private JobQueue jobQueue;
        private string name;
        private IPersistentStorage persistanSotrage;
        
        public JobConsumer(string name, JobQueue jobQueue, IPersistentStorage persistanStorage)
        {
            this.name = name;
            this.jobQueue = jobQueue;
            this.persistanSotrage = persistanStorage;
            
        }

        public void Run()
        {
            while (true)
            {
                Job job = jobQueue.GetNextAvailableJob();
                JobResult result = runJob(job);

                if (result == JobResult.DEFERRED)
                {
                    jobQueue.Add(job);
                }
                else
                {
                    if (result == JobResult.FAILURE)
                    {
                        job.Canceled();
                    }

                    if (job.IsPersistan)
                    {
                        persistanSotrage.Remove(job.PersistentId);
                    }

                    if (job.WakeLock && job.WakeLockTimeOut == 0)
                    {
                        job.Sleep();
                    }
                }

                if (job.GroupId != null)
                {
                    jobQueue.IsGroupIdAvaible(job.GroupId);
                }
            }
        }
        
        private JobResult runJob(Job job)
        {
            int retryCount = job.RetryCount;
            int runIteration = job.RunIteration;

            for (; runIteration < retryCount; runIteration++)
            {
                try
                {
                    job.Run();
                    return JobResult.SUCCESS;
                }
                catch (System.Exception exception)
                {
                    Log.Error(TAG, exception.ToString());

                    if (exception is RuntimeException)
                    {
                        throw (RuntimeException)exception;
                    }
                    else if (!job.ShouldRetry(exception))
                    {
                        return JobResult.FAILURE;
                    }
                    else if (!job.IsRequrementsMet)
                    {
                        job.RunIteration = (runIteration + 1);
                        return JobResult.DEFERRED;
                    }
                }
            }

            return JobResult.FAILURE;
        }
    }

    public enum JobResult
    {
        SUCCESS,
        FAILURE,
        DEFERRED
    }
}
