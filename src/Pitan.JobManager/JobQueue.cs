﻿using Pitan.JobManager.Requirements;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Pitan.JobManager
{
    public class JobQueue : IRequirementListener
    {
        public LinkedList<Job> jobQueue;
        public HashSet<string> activeGroupIds;

        #region Constructor

        public JobQueue()
        {
            this.jobQueue = new LinkedList<Job>();
            this.activeGroupIds = new HashSet<string>();
            this.activeGroupIds.Add(string.Empty);
        }

        #endregion

        #region Methods

        public void Add(Job job)
        {
            this.jobQueue.AddLast(job);
        }

        public void AddAll(IEnumerable<Job> jobs)
        {
            foreach (var item in jobs)
            {
                this.jobQueue.AddLast(item);
            }
        }

        public Job GetNextAvailableJob()
        {
            if (!this.jobQueue.Any())
            {
                return null;
            }

            var firstJob = this.jobQueue.FirstOrDefault(job => job.IsRequrementsMet && IsGroupIdAvaible(job.GroupId));
            this.jobQueue.Remove(firstJob);
            
            return firstJob;
        }
        
        public bool IsGroupIdAvaible(string groupId)
        {
            return activeGroupIds.Contains(groupId);
        }

        public void SetGroupIdUnavailable(string groupId)
        {
            if (!string.IsNullOrEmpty(groupId))
            {
                this.activeGroupIds.Add(groupId);
            }
        }

        public void SetGroupIdAveilable(string groupId)
        {
            
        }

        public void OnRequirementStatusChanged()
        {
            Monitor.PulseAll(this);
        }

        #endregion

    }
}
