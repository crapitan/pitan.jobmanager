﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pitan.JobManager
{
    public interface IJobManager
    {
        void Add(Job job);
        void OnRequirementStatusChanged();
    }
}
