﻿using System;

namespace Pitan.JobManager.Requirements
{

    public interface IRequirementListener
    {
        Action<bool> RequirementChanged { get; set; } 
    }
}
