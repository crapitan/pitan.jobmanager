﻿namespace Pitan.JobManager.Requirements
{
    public interface IRequirementProvider
    {
        IRequirementListener Listener { get; set; }
    }
}
