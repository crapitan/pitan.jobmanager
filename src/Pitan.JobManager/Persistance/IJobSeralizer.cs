﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pitan.JobManager.Persistance
{
    public interface IJobSeralizer
    {
        string Serialize(Job job);
        Job Deserialize(EncryptionKeys encryptionKeys, bool isEncrypted, string serialized);
    }
}
