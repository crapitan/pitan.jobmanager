﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pitan.JobManager.Persistance
{
    public interface IPersistentStorage
    {
        LinkedList<Job> Encrypted(EncryptionKeys keys);
        void Remove(long id);
        void Store(Job job);
        LinkedList<Job> UnencryptedJobs();
    }
}
