﻿using NUnit.Framework;
using Pitan.JobManager.Driod.Test.Jobs;
using Pitan.JobManager.Extensions;
using Pitan.JobManager.Droid;
using Pitan.JobManager.Droid.Extensions;
using Android.Content;

namespace Pitan.JobManager.Driod.Test
{
    [TestFixture]
    public class TestsSample
    {
        private Context context;

        public TestsSample(Context context)
        {
            this.context = context;
        }


        [SetUp]
        public void Setup() { }


        [TearDown]
        public void Tear() { }

        [Test]
        public void Pass()
        {

            var parameter = JobParameter.Builder().IsWakeLock(false).Retry();

            TestJob testJob = new TestJob(parameter, new Device.Device() );

            DriodJobManager jobManager = DriodJobManager.Build(this.context)
                                              .WithName("transient-test")
                                              .WithConsumerThreads(1)
                                              .Create();

            jobManager.Add(testJob);

            Assert.True(testJob.IsAdded());
            Assert.True(testJob.IsRan());
        }
    }
}