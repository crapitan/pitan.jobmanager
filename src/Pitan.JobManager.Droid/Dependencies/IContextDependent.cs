﻿using Android.Content;

namespace Pitan.JobManager.Droid
{
    public interface IContextDependent
    {
        void SetContext(Context context);
    }
}