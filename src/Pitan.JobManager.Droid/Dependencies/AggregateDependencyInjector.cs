using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Pitan.JobManager.Dependencies;
using Pitan.JobManager.Requirements;

namespace Pitan.JobManager.Droid.Dependencies
{
    public class AggregateDependencyInjector
    {
        private IDependencyInjector dependencyInjector;

        public AggregateDependencyInjector(IDependencyInjector dependencyInjector)
        {
            this.dependencyInjector = dependencyInjector;
        }

        public void InjectDependency(Context contenxt, Job job)
        {
            if (job is IContextDependent)
            {
                ((IContextDependent)job).SetContext(contenxt);
            }

            if (job.Requirements != null)
            {
                foreach (IRequirement item in job.Requirements)
                {
                    if (item is IContextDependent)
                    {
                        ((IContextDependent)item).SetContext(contenxt);
                    }
                }
            }

            dependencyInjector?.InjectDependecy(job);

            if (job.Requirements != null)
            {
                foreach (IRequirement item in job.Requirements)
                {
                    dependencyInjector.InjectDependecy(item);
                }
            }
        }
    }
}