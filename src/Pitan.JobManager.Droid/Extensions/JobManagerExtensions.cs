using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Pitan.JobManager.Droid.Extensions
{
    public static class JobManagerExtensions
    {
        public static DriodJobManager.Builder WithName(this DriodJobManager.Builder builder, string name)
        {
            builder.Name = name;
            return builder;
        }

        public static DriodJobManager.Builder WithConsumerThreads(this DriodJobManager.Builder builder, int consumers)
        {
            builder.Consumers = consumers;
            return builder;
        }

        public static DriodJobManager.Builder WithRequrementProvider(this DriodJobManager.Builder builder, Requirements.IRequirementProvider requrement)
        {
            builder.RequirementProviders = builder.RequirementProviders ?? new List<Requirements.IRequirementProvider>();
            builder.RequirementProviders.Add(requrement);
            return builder;
        }

    }
}