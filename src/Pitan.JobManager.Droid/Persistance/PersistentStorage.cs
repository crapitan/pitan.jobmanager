using System;
using System.Collections.Generic;
using Android.Content;
using Pitan.JobManager.Persistance;
using Pitan.JobManager.Droid.Dependencies;
using Android.Database.Sqlite;
using Android.Database;
using Android.Util;
using Java.IO;
using System.Threading.Tasks;

namespace Pitan.JobManager.Droid.Persistance
{
    public class PersistentStorage : IPersistentStorage
    {
        private static string TABLE_NAME = "queue";
        private static string ID = "_id";
        private static string ITEM = "item";
        private static int DATABASE_VERSION = 1;
        private static string ENCRYPTED = "encrypted";
        private static string DATABASE_CREATE = string.Format("CREATE TABLE %s (%s INTEGER PRIMARY KEY, %s TEXT NOT NULL, %s INTEGER DEFAULT 0);", TABLE_NAME, ID, ITEM, ENCRYPTED);

        private DatabaseHelper databaseHelper;
        private Context context;
        private IJobSeralizer jobSerializer;
        private AggregateDependencyInjector dependencyInjector;

        public PersistentStorage(Context context, string name, IJobSeralizer jobSeralizer, AggregateDependencyInjector dependencyInjector)
        {
            this.databaseHelper = new DatabaseHelper(this.context, "JobQueue-" + name);
            this.context = context;
            this.jobSerializer = jobSeralizer;
            this.dependencyInjector = dependencyInjector;
        }

        public void Store(Job job)
        {
            ContentValues contentValues = new ContentValues();
            contentValues.Put(ITEM, jobSerializer.Serialize(job));
            contentValues.Put(ENCRYPTED, job.EncryptionKeys != null);

            long id = this.databaseHelper.WritableDatabase.Insert(TABLE_NAME, null, contentValues);
            job.PersistentId = id;
        }

        public Task<Job> StoreAsync(Job job)
        {
            ContentValues contentValues = new ContentValues();
            contentValues.Put(ITEM, jobSerializer.Serialize(job));
            contentValues.Put(ENCRYPTED, job.EncryptionKeys != null);

            long id = this.databaseHelper.WritableDatabase.Insert(TABLE_NAME, null, contentValues);
            job.PersistentId = id;

            return Task.FromResult(job);
        }
        
        public void Remove(long id)
        {
            databaseHelper.WritableDatabase
                          .Delete(TABLE_NAME, ID + " = ?", new string[] { id.ToString() });
        }

        public LinkedList<Job> UnencryptedJobs()
        {
          return jobs(null, ENCRYPTED + " = 0");
        }

        public LinkedList<Job> Encrypted(EncryptionKeys keys)
        {
            return jobs(keys, ENCRYPTED + " = 1");
        }

        private LinkedList<Job> jobs(EncryptionKeys keys, string where)
        {
            var results = new LinkedList<Job>();

            SQLiteDatabase database = databaseHelper.ReadableDatabase;
            ICursor cursor = null;

            try
            {
                cursor = database.Query(TABLE_NAME, null, where, null, null, null, ID + " ASC", null);

                while (cursor.MoveToNext())
                {
                    long id = cursor.GetLong(cursor.GetColumnIndexOrThrow(ID));
                    String item = cursor.GetString(cursor.GetColumnIndexOrThrow(ITEM));
                    bool encrypted = cursor.GetInt(cursor.GetColumnIndexOrThrow(ENCRYPTED)) == 1;

                    try
                    {
                        Job job = jobSerializer.Deserialize(keys, encrypted, item);

                        job.PersistentId = id;
                        job.EncryptionKeys = keys;
                        dependencyInjector.InjectDependency(context, job);

                        results.AddLast(job);
                    }
                    catch (IOException e)
                    {
                        Log.Error("PersistentStore", e.Message);
                        Remove(id);
                    }
                }
            }
            finally
            {
                if (cursor != null)
                    cursor.Close();
            }

            return results;
        }

        private class DatabaseHelper : SQLiteOpenHelper
        {
            public DatabaseHelper(Context context, string name) : base(context, name, new SQLiteCursorFactory(true), DATABASE_VERSION)
            {

            }

            public override void OnCreate(SQLiteDatabase db)
            {
                db.ExecSQL(DATABASE_CREATE);
            }

            public override void OnUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
            {
                // Do nothing
            }
        }

        internal sealed class SQLiteCursorFactory : Java.Lang.Object, SQLiteDatabase.ICursorFactory
        {
            private readonly bool _debugQueries;

            public SQLiteCursorFactory(bool debugQueries)
            {
                _debugQueries = debugQueries;
                Log.Debug("SQLiteCursorFactory", "SQLiteCursorFactory instantiated, debugging queries = {0}", debugQueries);
            }

            public ICursor NewCursor(SQLiteDatabase db, ISQLiteCursorDriver masterQuery, string editTable, SQLiteQuery query)
            {
                Log.Debug("SQLiteCursorFactory", "Creating new cursor");
                if (_debugQueries)
                {
                    Log.Debug("SQLiteCursorFactory", "Query: {0}", masterQuery.ToString());
                }
                return new SQLiteCursor(masterQuery, editTable, query);
            }

            ICursor SQLiteDatabase.ICursorFactory.NewCursor(SQLiteDatabase db, ISQLiteCursorDriver masterQuery, string editTable, SQLiteQuery query)
            {
                throw new NotImplementedException();
            }
        }
    }
}