﻿using Android.Content;
using Android.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pitan.JobManager.Requirements
{
    public class NetworkRequirement : IRequirement
    {
        private Context context;

        public NetworkRequirement(Context context)
        {
            this.context = context;
        }

        public bool IsPresent()
        {
            ConnectivityManager connectivityManager = (ConnectivityManager)this.context.GetSystemService("ConnectivityManager");
            NetworkInfo activeConnection = connectivityManager.ActiveNetworkInfo;
            bool isOnline = (activeConnection != null) && activeConnection.IsConnected;

            return isOnline;
        }
    }

    public class NetworkRequrementProvider : IRequirementProvider
    {
        private Context context;
        private NetworkRequirement networkRequired;

        public NetworkRequrementProvider(Context context)
        {
            this.context = context;
            this.networkRequired = new NetworkRequirement(context);
            this.context.RegisterReceiver(new NetworkChangedReciver(this.connected), new IntentFilter(ConnectivityManager.ConnectivityAction));
        }

        private void connected()
        {
            if (this.networkRequired.IsPresent())
            {
                this.Listener?.RequirementChanged(true);
            } 
        }

        public IRequirementListener Listener { get; set; }
    }

    public class NetworkChangedReciver : BroadcastReceiver
    {
        private Action p;

        public NetworkChangedReciver(Action p)
        {
            this.p = p;
        }

        public override void OnReceive(Context context, Intent intent)
        {
            p.Invoke();
        }
    }


}
