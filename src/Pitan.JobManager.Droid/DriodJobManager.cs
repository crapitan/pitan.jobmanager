using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content;

using Pitan.JobManager.Requirements;
using Pitan.JobManager.Dependencies;
using Pitan.JobManager.Persistance;
using Pitan.JobManager.Droid.Persistance;
using Pitan.JobManager.Droid.Dependencies;
using Java.Util.Concurrent.Atomic;
using Java.Util.Concurrent;
using System.Threading;
using Android.Util;

namespace Pitan.JobManager.Droid
{
    public class DriodJobManager : IRequirementListener, IJobManager
    {
        private JobQueue jobQueue = new JobQueue();
        private IExecutorService eventExecutor = Executors.NewSingleThreadExecutor();
        private AtomicBoolean hasLoadedEncrypted = new AtomicBoolean(false);

        private Context context;
        private AggregateDependencyInjector dependencyInjector;
        private PersistentStorage persistentStorage;
        private List<IRequirementProvider> requirementProviders;

        public DriodJobManager(Context context, string name,
                     List<IRequirementProvider> requirementProviders,
                     IDependencyInjector dependencyInjector,
                     IJobSeralizer jobSerializer, int consumers)
        {

            this.context = context;
            this.dependencyInjector = new AggregateDependencyInjector(dependencyInjector);
            this.persistentStorage = new PersistentStorage(context, name, jobSerializer, this.dependencyInjector);
            this.requirementProviders = requirementProviders;

            if (requirementProviders != null && requirementProviders.Any())
            {
                foreach (IRequirementProvider provider in requirementProviders)
                {
                    provider.Listener = this;
                }
            }

            for (int i = 0; i < consumers; i++)
            {
                Thread thread = new Thread(() => new JobConsumer("JobConsumer-" + i, jobQueue, persistentStorage).Run());
                thread.Start();
            }
        }

        public async void Add(Job job)
        {
            try
            {
                if (job.WakeLock)
                {
                    job.Device = new Device.AndroidDevice(this.context);
                }

                if (job.IsPersistan)
                {
                    job = await this.persistentStorage.StoreAsync(job);
                }

                dependencyInjector.InjectDependency(this.context, job);

                job.Added();
                jobQueue.Add(job);


            }
            catch (Exception exp)
            {
                job.Canceled();
                Log.Error(this.GetType().ToString(), exp.ToString());
            }
        }

        public void OnRequirementStatusChanged()
        {
            jobQueue.OnRequirementStatusChanged();
        }
        
        public static Builder Build(Context context)
        {
            return new Builder(context);
        }

        public class Builder
        {
            public Builder(Context context)
            {
                this.context = context;
            }

            public Context context { get; set; }

            public PersistentStorage PersistentStorage { get; set; }
            public List<IRequirementProvider> RequirementProviders { get; set; }

            public string Name { get; set; }
            public IJobSeralizer JobSerializer { get; set; }
            public int Consumers { get; set; }
            public IDependencyInjector DependencyInjector { get; set; }

            public DriodJobManager Create()
            {
                this.RequirementProviders = this.RequirementProviders ?? new List<IRequirementProvider>();

                return new DriodJobManager(this.context, this.Name, this.RequirementProviders, this.DependencyInjector, this.JobSerializer, this.Consumers);
            }
        }

    }
}