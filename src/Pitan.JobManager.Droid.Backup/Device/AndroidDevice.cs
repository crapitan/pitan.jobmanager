using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Pitan.JobManager.Device;

namespace Pitan.JobManager.Droid.Device
{
    class AndroidDevice : IDeviceSpecific
    {
        private PowerManager.WakeLock wakeLock;

        public AndroidDevice(Context context)
        {
            PowerManager pm = (PowerManager)context.GetSystemService(Context.PowerService);
            this.wakeLock = pm.NewWakeLock(WakeLockFlags.ScreenDim, "JobManager");
        }

        public void WakeUp()
        {
            wakeLock.Acquire();
        }

        public void GoToSleep()
        {
            wakeLock.Release();
        }
    }
}