﻿
using Android.App;
using Android.Content.PM;
using Android.OS;
using Pitan.JobManager.Droid.Extensions;
using Pitan.JobManager.Droid;
using Pitan.JobManager.Droid.Device;
using Pitan.JobManager.Device;
using Android.Content;
using Android.Net;
using Pitan.JobManager.Requirements;

namespace Pitan.JobManager.Example.Droid
{
    [Activity(Label = "Pitan.JobManager.Example", Icon = "@drawable/icon", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);

            // Lets create an android device
            IDevice device = new AndroidDevice(this);  
            
            // The manager class uses some android spcific stuff therefore DroidJobManager                  
            IJobManager jobManager = DriodJobManager.Build(this)
                                              .WithName("download-test")
                                              .WithConsumerThreads(1)
                                              .WithRequrementProvider(new NetworkRequrementProvider(this))
                                              .Create();
            
            LoadApplication(new App(jobManager, device));
        }
    }
}